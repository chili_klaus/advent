use crate::helpers;

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE_1: &str = "0,3,6";

    #[test]
    fn test_solve_1() {
        assert_eq!(solve_1(EXAMPLE_1), 436);
    }
}

fn solve(input: &str, size: usize) -> usize {
    let mut occurences: Vec<usize> = vec![0; size];
    let mut last_num = 0;
    let mut game_start_idx = 0;
    let nums = input.split(",").map(|c| c.parse::<usize>().unwrap());
    for (i, num) in nums.enumerate().map(|(i, num)| (i + 1, num)) {
        occurences[num] = i;
        last_num = num;
        game_start_idx = i;
    }
    for i in game_start_idx..size {
        if occurences[last_num] != 0 {
            let last_occurence = occurences[last_num];
            occurences[last_num] = i;
            last_num = i - last_occurence;
        } else {
            occurences[last_num] = i;
            last_num = 0;
        }
    }
    return last_num;
}

fn solve_1(input: &str) -> usize {
    return solve(input, 2020);
}
fn solve_2(input: &str) -> usize {
    return solve(input, 30000000);
}

pub fn run(input: &str) {
    helpers::run_benchmarked(|| println!("{}", solve_1(input)));
    helpers::run_benchmarked(|| println!("{}", solve_2(input)));
}
