use regex::Regex;
use std::collections::HashSet;

use crate::helpers;

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE_1: &str = "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";

    #[test]
    fn test_solve_1() {
        assert_eq!(solve_1(EXAMPLE_1), 71);
    }
}

fn parse_rule(input: &str) -> (&str, (i32, i32), (i32, i32)) {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(.+): (\d+)\-(\d+) or (\d+)\-(\d+)$").unwrap();
    }
    let cap = RE.captures(input).unwrap();
    return (
        cap.get(1).unwrap().as_str(),
        (
            cap[2].parse::<i32>().unwrap(),
            cap[3].parse::<i32>().unwrap(),
        ),
        (
            cap[4].parse::<i32>().unwrap(),
            cap[5].parse::<i32>().unwrap(),
        ),
    );
}

fn parse_ticket(input: &str) -> Vec<i32> {
    return input
        .split(",")
        .map(|num| num.parse::<i32>().unwrap())
        .collect();
}

fn parse(input: &str) -> (Vec<(&str, (i32, i32), (i32, i32))>, Vec<i32>, Vec<Vec<i32>>) {
    let mut splat = input.split("\n\n");
    let rules = splat.next().unwrap().split("\n").map(parse_rule).collect();
    let ticket = parse_ticket(splat.next().unwrap().split("\n").last().unwrap());
    let nearby_tickets = splat
        .next()
        .unwrap()
        .split("\n")
        .skip(1)
        .map(parse_ticket)
        .collect();
    return (rules, ticket, nearby_tickets);
}

fn invalid_field(rules: &Vec<(&str, (i32, i32), (i32, i32))>, ticket: &Vec<i32>) -> Option<i32> {
    for field in ticket {
        let mut field_valid = false;
        for (_, (min_1, max_1), (min_2, max_2)) in rules {
            if (field >= min_1 && field <= max_1) || (field >= min_2 && field <= max_2) {
                field_valid = true;
                break;
            }
        }
        if !field_valid {
            return Some(*field);
        }
    }
    return None;
}

fn solve_1(input: &str) -> i32 {
    let (rules, _, nearby_tickets) = parse(input);
    return nearby_tickets
        .iter()
        .filter_map(|ticket| invalid_field(&rules, ticket))
        .sum();
}

fn decipher_fields(
    rules: &Vec<(&str, (i32, i32), (i32, i32))>,
    ticket: &Vec<i32>,
) -> Option<Vec<HashSet<usize>>> {
    let mut possible_fields: Vec<HashSet<usize>> = vec![];
    for field in ticket {
        let mut field_rules = HashSet::new();
        for (i, (_, (min_1, max_1), (min_2, max_2))) in rules.iter().enumerate() {
            if (field >= min_1 && field <= max_1) || (field >= min_2 && field <= max_2) {
                field_rules.insert(i);
            }
        }
        if field_rules.is_empty() {
            return None;
        }
        possible_fields.push(field_rules);
    }
    return Some(possible_fields);
}

fn solve_2(input: &str) -> u64 {
    let (rules, my_ticket, nearby_tickets) = parse(input);
    let mut possible_fields: Vec<HashSet<usize>> = decipher_fields(&rules, &my_ticket).unwrap();
    for ticket in nearby_tickets {
        match decipher_fields(&rules, &ticket) {
            Some(ticket_possible_fields) => {
                for i in 0..possible_fields.len() {
                    possible_fields[i].retain(|field| ticket_possible_fields[i].contains(field));
                }
            }
            None => (),
        };
    }
    let mut enumerated_fields: Vec<(usize, &HashSet<usize>)> =
        possible_fields.iter().enumerate().collect();
    enumerated_fields.sort_by(|a, b| a.1.len().cmp(&b.1.len()));
    let mut known_fields: HashSet<usize> = HashSet::new();
    return enumerated_fields
        .iter()
        .filter_map(|(field_idx, possible_rule_indexes)| {
            let rule_idx = possible_rule_indexes
                .difference(&known_fields)
                .next()
                .unwrap()
                .clone();
            known_fields.insert(rule_idx);
            if rules[rule_idx].0.starts_with("departure") {
                Some(my_ticket[*field_idx] as u64)
            } else {
                None
            }
        })
        .product();
}

pub fn run(input: &str) {
    helpers::run_benchmarked(|| println!("{}", solve_1(input)));
    helpers::run_benchmarked(|| println!("{}", solve_2(input)));
}
